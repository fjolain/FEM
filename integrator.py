# -*- coding: utf-8 -*-
import numpy as np
class IntegratorBuilder :
    """ Construit une méthode d'intégration selon un patern Builder"""
    
    @classmethod
    def build(cls, choice, simplex) :
        
        # Calcul des paramètres d'intégration
        measure = simplex.getVolume()
        if choice == 'linear' :
            weight = np.array([1.], dtype=float)
            points = np.array([simplex.getCenterOfGravity()], dtype=float)
        
        elif choice == 'quadratic' :
            # Pour dimension 2D ou plus formules des milieux
            if simplex.dim >= 2 :
                weight = np.ones(simplex.dim+1, dtype=float)/(simplex.dim+1)
                points = np.array(simplex.getMiddles(), dtype=float)
            
            # Si segment on utilise la formule de Simpson
            elif simplex.dim == 1 :
                weight = np.array([1, 4, 1])/6
                points = np.array([simplex.xs[:, 0], simplex.getCenterOfGravity(), simplex.xs[:, 1]], dtype=float)
        else :
            print("méthode d'intégration non trouvée")
            
        return lambda G : IntegratorBuilder.integrate(measure, weight, points, G)
    
    @classmethod    
    def integrate(cls, measure, weight, points, G):
        """ intègre la fonction G sur l'élément de référence """
        I = 0
        for i in range(0, weight.size) :
            I += weight[i]*G(points[i])
        return I*measure