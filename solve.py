# -*- coding: utf-8 -*-

""" fichier Solve """


import numpy as np
import scipy as sp
from scipy.sparse.linalg import spsolve
from scipy.sparse import lil_matrix

from element import RefElement, Element, Simplex

class Forms :
    """ Formes varitionnelles"""
    
    def __init__(self, a, b, c, q, d, beta):
        """ Forme varitionnelles :
            -d(a*du) + b*du + c*u = q
            
            Neumann     : a*du.n + d*u = beta
            Direchlet   :  u = alpha
        """
        
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.q = q
        self.d = d
        self.beta = beta
        
        
    def omegaA(self, e) :
        """ Forme Ae de l'élément sous l'intégrale"""
        
        return lambda x : e.Je*(
            np.dot(e.gradPhi(x).T, np.dot(self.a(e.coord(x)), e.gradPhi(x))) + 
            np.dot(e.phi(x), np.dot(self.b(e.coord(x)).T,e.gradPhi(x))) + 
            self.c(e.coord(x))*np.dot(e.phi(x),e.phi(x).T) )
            
    def omegaF(self, e) :
        """ Forme Fe de l'élément sous l'intégrale"""
        return lambda x : e.Je*(self.q(e.coord(x))*e.phi(x))
        
        
    def neumannA(self, e) :
        """ Forme Ab de l'élément sous l'intégrale"""
        return lambda x :e.Je*(self.d(e.coord(x))*e.phi(x).dot(e.phi(x).T))
        
    def neumannF(self, e) :
        """ Forme Fb de l'éléent sous l'intégrale"""
        return lambda x: e.Je*(self.beta(e.coord(x))*e.phi(x))
            
            
class Solve :
    """ Résolution d'équation """
      
    def __init__(self, equation) :
        
        self.equation = equation        
        self.coord = sp.loadtxt(equation['coord'], dtype=float)        
        self.size, self.dim = self.coord.shape
        
        self.forms = Forms(equation['a'], equation['b'], equation['c'], equation['q'], equation['d'], equation['beta'])
        
        self.A = lil_matrix((self.size, self.size), dtype=complex)
        self.F = lil_matrix((self.size, 1), dtype=complex)
        
    
    def omegaAdd(self, intg, poly) :
        """Ajoute la contribution des formes sur omega"""
        
        elements = sp.loadtxt(self.equation['elements']).astype(int)
        ref = RefElement(self.dim, intg, poly)
        
        for ddl in elements :
            xe = np.array(self.coord[ddl]).T
            e = Element(Simplex(xe), ref)
            
            Ae = ref.integrate(self.forms.omegaA(e))
            self.A[np.ix_(ddl, ddl)] += Ae
            
            Fe = ref.integrate(self.forms.omegaF(e))
            #print("Fe = ", Fe)
            self.F[ddl] += Fe
            
            
    def neumannAdd(self, intg, poly) :
        """ Ajoute la contribution des formes sur neumann"""
        if self.equation['neumann'] == '' :
            return False
        
        Ap = sp.sparse.lil_matrix((self.size, self.size), dtype=complex)
        
        neumann = sp.loadtxt(self.equation['neumann']).astype(int)
        ref = RefElement(self.dim-1, intg, poly)
        
        for ddl in neumann :
            xe = np.array(self.coord[ddl]).T
            e = Element(Simplex(xe), ref)
            
            Ab = ref.integrate(self.forms.neumannA(e))
            self.A[np.ix_(ddl, ddl)] += Ab#*55.866
            
            Ap[np.ix_(ddl, ddl)] += Ab
            
            #print(ddl,'\n', Ab, '\n\n')
            
            Fb = ref.integrate(self.forms.neumannF(e))
            self.F[ddl] += Fb
            
        return Ap
            
            
    def dirichletAdd(self) :
        """ Ajoute la contribution de dirichlet """
        if self.equation['dirichlet'] == '' :
            return False
        
        dirichlet = sp.loadtxt(self.equation['dirichlet'], dtype=int)

        for noeud in dirichlet :
            self.A[noeud] = np.zeros(self.size)
            self.A[noeud, noeud] = 1
            
            self.F[noeud] = self.equation['alpha'](self.coord[noeud])
            
    def solve(self) :
        self.sol = spsolve(self.A, self.F).T
        return self.sol
