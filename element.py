# -*- coding: utf-8 -*-

import numpy as np
from interpolator import InterpolatorBuilder
from integrator import IntegratorBuilder
from math import factorial

class RefElement :
    """ Element de référence comprenant les polynomes interpolateurs et les methode d'intégration"""
    
    def __init__(self, dimension, integrate, interpolator) :
        """ xs          : sommets de l'élement [[x1, ... xn], [y1, ...yn], ... ]
            intetgrate  : méthodes d'intégration utilisée ('lineare', 'quadratic', 'cubic')
        """
        
        # Construit un simplexe par la dimension n souhaitée
        # xs = [ I_n   0_nx1]
        xs = np.concatenate((np.eye(dimension), np.zeros((dimension, 1))), axis=1)
        simplex = Simplex(xs)

        # Ajout des polynomes interpolateurs
        N = InterpolatorBuilder.build(interpolator, simplex)
        
        self.phi = N.phi
        self.gradPhi = N.gradPhi
        
        # Ajoute une méthode d'intégration 
        self.integrate = IntegratorBuilder.build(integrate, simplex)
        
        
class Element :
    """ Element standard"""
    
    def __init__(self, simplex, ref) :
        """ simplex : le simplex décrivant l'élément
            ref     : élément de référence du problème
        """
        self.ref = ref
        self.simplex = simplex
        self.Te = np.add(simplex.xs[:, :-1], -simplex.xs[:, -1].reshape(simplex.dim_work, 1))
        
        # Simplexe 2D ou plus
        if simplex.dim >= 2 :
            self.Te_inv = np.linalg.inv(self.Te)
            self.Je = np.linalg.det(self.Te)
            if self.Je < 0 :
                print("Le jacobien est négatif :", simplex.xs)
        # Si Simplexe 1D
        if simplex.dim == 1 :
            #self.Te_inv = 1/self.Te
            self.Je = np.linalg.norm(self.Te)
            
    def __str__(self) :
        des  = "points : \n"+str(self.simplex.xs)+"\n"
        des += "Te =\n"+str(self.Te)+"\n"+"Je = "+str(self.Je)+"\n"
        return des
            
    def coord(self, x) :
        return self.Te.dot(x) + self.simplex.xs[:, -1]
        
    def phi(self, x) :
        """ Calcul phi par l'élement de référence"""
        return self.ref.phi(x)
        
    def gradPhi(self, x=0) :
        """ Calcul grad phi par la transfomé"""
        return self.Te_inv.T.dot(self.ref.gradPhi(x))#/self.Je
            
        
        

class Simplex :
    """ Simplexe à N dimensions"""
    
    def __init__(self, xs) :
        """ xs  : sommets du simplexe [[x1, ... xn], [y1, ...yn], ... ]"""
        
        # Dimension de travail dans lequel les coordonnées de l'éléments sont données
        self.dim_work = len(xs)
        # Dimension de l'élément. Peut être différent de la dimensionde travail
        # dans le cas d'élément de Neumann
        self.dim = len(xs[0]) - 1
        
        self.xs = np.array(xs)
        # Réorganise les points 
        # [[x1, ... xn], [y1, ...yn], ... ] --> [[x1, y1, ...], ... [xn, yn, ...]]
        self.points = self.xs.T
        
        
    def getCenterOfGravity(self) :
        """ Calcul le centre de gravité du simplexe """
        #            _n_        
        #         1  \    Point_k
        # CoG =  --- /__  
        #         n   k
        return np.sum(self.points, axis=0, dtype=float)/len(self.points)
        
    def getMiddles(self) :
        """ Retourne les milieux des segments """
        middles = []
        for i in range(0, len(self.points)-1) :
            middles.append( (self.points[i]+self.points[i+1])/2 )
        # Add the last middle between first and last point
        middles.append( (self.points[0]+self.points[-1])/2)
        
        return middles
        
    def getVolume(self) :
        """ Retourne l'hypervolume du simplex si simplexe STANDARD"""                 
        return 1.0/factorial(self.dim)
        