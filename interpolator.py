# -*- coding: utf-8 -*-
import numpy as np
from abc import ABCMeta, abstractmethod

class InterpolatorBuilder :
    """ Selectionneur d'un polynome interpolator"""
    
    @classmethod
    def build(clc, choice, simplex) :
        if choice == 'P1' : 
            return P1(simplex.xs)
        else : 
            print("polynome interpolateur non trouvé")
            return None

class Interpolator(metaclass=ABCMeta) :
    
    @abstractmethod
    def phi(self, x) :
        pass
    
    @abstractmethod
    def gradPhi(self, x=0):
        pass

class P1(Interpolator) :
    """ interpolateur P1 """
    
    def __init__(self, xs) :
        """ xs          : sommets de l'élement [[x1, ... xn], [y1, ...yn], ... ]"""
        self.Le = np.concatenate( (np.ones((1, len(xs[0]))), xs), axis=0)
        self.Le_inv = np.linalg.inv(self.Le)
        self.normE = abs(np.linalg.det(self.Le)*0.5)
        self.grad = self.Le_inv[:, 1:].T
        
    def phi(self, x) :
        """ Donne la fonction d'interpolation N_i """
        return self.Le_inv.dot(np.concatenate(([[1]], [x]), axis=1).T)
        
    def gradPhi(self, x=0) :
        """ Donne le gradien d'interpolation P1"""
        return self.grad
        