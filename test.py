# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pylab as plt
from solve import Solve

equation = {
    'a' : lambda x : np.array([[x[0]+1, x[0]], [0, x[0]]]),
    'b' : lambda x : np.array([[1], [x[1]]]),
    'c' : lambda x : x[0],
    'q' : lambda x : 2 - 2*x[1] + x[0]*(x[0] -2*x[1]),

    'alpha' : lambda x : x[0] - 2*x[1],
    'd' : lambda x : x[0],
    'beta' : lambda x :2*x[0] + x[0]*(x[0] -2*x[1]),

    'coord' : 'CoordT.dat',
    'elements' : 'ElementsT.dat',
    'neumann' : 'NeumannT.dat',
    'dirichlet' : 'DirichletT.dat'
}
    

s = Solve(equation)
s.omegaAdd('quadratic', 'P1')
s.neumannAdd('quadratic', 'P1')
s.dirichletAdd()

X_sol = s.solve()

X_th = [p[0] -2*p[1] for p in s.coord]
err = 100*np.linalg.norm(X_sol - X_th)/np.linalg.norm(X_th)
print("Pourcentage d'erreur ", err, "%")