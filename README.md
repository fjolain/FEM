### Introduction
This program is a finit element solver, it currently resolve all scalar function in N dimension.

You can find a program test at `Test.ipynb`.

More detail about program architecture and a demonstration on acoustic problem are in `rapport_acoustique.pdf`
